public abstract class Person {
    private String name;
    private int age;
    private String socialSecurityNumber;

    // Constructeur
    
    public Person(String name, int age, String socialSecurityNumber) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    // Getters et setters

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int newAge) {
        this.age = newAge;
    }

    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }
}
