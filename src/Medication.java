public class Medication {
    private String name;
    private String dosage;

    // Constructeur

    public Medication(String name, String dosage) {
        this.name = name;
        this.dosage = dosage;
    }

    // Getters et setters

    public String getName() {
        return this.name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public String getDosage() {
        return this.dosage;
    }

    public void setDosage(String newDosage) {
        this.dosage = newDosage;
    }

    // Méthodes

    public String getInfos() {
        return "- " + this.name + " : " + this.dosage;
    }
}
