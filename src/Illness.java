import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList = new ArrayList<>();

    // constructeur

    public Illness(String name) {
        this.name = name;
    }

    // getters

    public String getName() {
        return this.name;
    }

    public ArrayList<Medication> getMedicationList() {
        return this.medicationList;
    }

    // Méthodes

    public void addMedication(Medication medication) {
        this.medicationList.add(medication);
    }

    public String getInfo() {
        String medicationInfo = "";
        for (Medication medication : medicationList) {
            medicationInfo += medication.getInfos() + "\n";
        }
        return "- " + this.name + "\nRecommanded medication dosage :\n" + medicationInfo;
    }

}
