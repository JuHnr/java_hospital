import java.util.ArrayList;

public class Patient extends Person {
    private String patientId;
    private ArrayList<Illness> illnessList = new ArrayList<>();

    // constructeur
    public Patient(String name, int age, String socialSecurityNumber, String patientId) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
    }

    // getters

    public String getPatientId() {
        return this.patientId;
    }

    public ArrayList<Illness> getIllnessList() {
        return this.illnessList;
    }

    // Méthodes

    public void addIllness(Illness illness) {
        illnessList.add(illness);
    }

    public String getInfo() {

        String allIllnessInfo = "";
        for (Illness illness : illnessList) {
            allIllnessInfo += illness.getInfo() + "\n\n";
        }

        return "Patient informations : \n\nPatient ID : " + this.patientId + "\nName : " + getName() + "\nAge : "
                + getAge() + "\nSocial Security Number : " + getSocialSecurityNumber() + "\n\nIllness(es) : \n"
                + allIllnessInfo;
    }

}
