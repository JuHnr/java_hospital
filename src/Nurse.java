public class Nurse extends MedicalStaff {

    public Nurse(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Nurse " + getName() + " cares for " + patient.getName());
    }

    @Override
    public void getRole() {
        System.out.println("Nurse");
    }
    
}
