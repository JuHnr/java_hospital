public class Hospital {
    public static void main(String[] args) {

        /* Instanciations */

        // Medication
        Medication aspirin500 = new Medication("Aspirin", "500mg");
        Medication doliprane1000 = new Medication("Doliprane", "1000mg");

        // Illness
        Illness flu = new Illness("Flu");
       
        // Patient
        Patient patientA = new Patient("Georges Daniel", 40, "32215463-1235", "GD2358");

        // Doctor et Nurse
        Doctor doctorA = new Doctor("Grey", 52, "321251254-2235", "DG5621", "General");
        Nurse nurseA = new Nurse("Carla", 32, "325565832-1225", "NC3265");


        /* Test des méthodes */

        flu.addMedication(aspirin500);
        flu.addMedication(doliprane1000);
        patientA.addIllness(flu);

        System.out.println(patientA.getInfo());

        doctorA.getRole();
        nurseA.getRole();

        nurseA.careForPatient(patientA);
        doctorA.careForPatient(patientA);

        doctorA.recordPatientVisit("Georges has a flu.");

    }
}
